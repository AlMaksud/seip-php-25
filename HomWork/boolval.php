<!DOCTYPE html>
<html>
    <head>
        <title>Variable Handling Function</title>
    </head>
    
    <body>
       
        <h2>
            This page is for boolval practice...
        </h2>
        <?php
           echo '0:     ' . (boolval(0) ? 'true':'false')."<br>";
           echo '42:    ' . (boolval(42) ? 'true':'false')."<br>";
           echo '0.0:   ' . (boolval(0.0) ? 'true':'false')."<br>";
           echo '4.2:   ' . (boolval(4.2) ? 'true':'false')."<br>";
           echo '"":    ' . (boolval("") ? 'true':'flase') . "<br>";
           echo '"String:"'.(boolval("String") ? 'true':'flase'). "<br>";
           echo '"0":   ' . (boolval("0") ? 'true':'flase') . "<br>";
           echo '"1":   '.(boolval("0") ? 'true':'false') . "<br>";
           echo '[1, 2]:   '.(boolval([1, 2]) ? 'true' : 'false') ."<br>";
           echo '[]:    ' .(boolval([]) ? 'true ':'false') . "<br>";
           echo 'stdClass:  '.(boolval(new stdClass) ? 'true':'false');
           
        ?>
    </body>
</html>



