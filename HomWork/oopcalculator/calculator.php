<?php
	
	include_once('calculate.php');



	if ( isset($_POST['cal'])) {

		if ( (!empty($_POST['num1']) && !empty($_POST['num2']) ) && ( is_numeric($_POST['num1']) && is_numeric($_POST['num2'])) ) {

			$a = $_POST['num1'];
			$b = $_POST['num2'];
			
			$cal = new calculation();

			//adding two input using add function from calculation class 
			$addition_result = $cal->add($a , $b);
			echo 'Addition is:' . $addition_result;
			echo '<br>';

			//subtracting two input using sub function from calculation class 
			$subtraction_result = $cal->sub($a , $b);
			echo 'Substraction is:' . $subtraction_result;
			echo '<br>';

			//Multiplying two input using mult function from calculation class
			$multiplication_result = $cal->mult($a , $b);
			echo 'Multiplication is:' . $multiplication_result;
			echo '<br>';

			//Divisioning two input using div functionfrom calculation class
			$division_result = $cal->div($a , $b);
			echo 'Division is:' . $division_result;

		}
		else{
			$input_type1 = gettype($_POST['num1']);
			$input_type2 = gettype($_POST['num2']);

			if( ($input_type1 == 'string') &&($input_type2== 'string')){
				echo 'Invalid input';
			}
		}

	}
	else{
		echo 'Cal is not set';
	} 

?>